# translation of ktimetracker.po to Icelandic
# Gudmundur Erlingsson.
# Copyright (C) 1999 Gudmundur Erlingsson.
#
# Gudmundur Erlingsson <gerl@vortex.is>, 1999.
# Svanur Palsson <svanur@tern.is>, 2004.
# Þröstur Svanbergsson <throstur@bylur.net>, 2004.
# Sveinn í Felli <sveinki@nett.is>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: ktimetracker\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-29 00:44+0000\n"
"PO-Revision-Date: 2009-07-28 20:22+0000\n"
"Last-Translator: Sveinn í Felli <sveinki@nett.is>\n"
"Language-Team: Icelandic <kde-isl@molar.is>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"\n"
"\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""
"Guðmundur Erlingsson, Ingimar Róbertsson, Guðjón I. Guðjónsson, Pjetur G. "
"Hjaltason, Svanur Pálsson"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"gerl@vortex.is, iar@pjus.is, gudjonh@hi.is, pjetur@pjetur.net, "
"svanurpalsson@hotmail.com"

#: dialogs/edittimedialog.cpp:42
#, fuzzy, kde-format
#| msgid "Edit Task"
msgctxt "@title:window"
msgid "Edit Task Time"
msgstr "Breyta verki"

#: dialogs/edittimedialog.cpp:50 dialogs/taskpropertiesdialog.cpp:56
#, fuzzy, kde-format
#| msgid "Task"
msgctxt "@title:group"
msgid "Task"
msgstr "Verkefni"

#: dialogs/edittimedialog.cpp:61 dialogs/taskpropertiesdialog.cpp:65
#, fuzzy, kde-format
#| msgid "Task Name"
msgid "Task Name:"
msgstr "Nafn verkefnis"

#: dialogs/edittimedialog.cpp:68 dialogs/taskpropertiesdialog.cpp:70
#, kde-format
msgid "Task Description:"
msgstr ""

#: dialogs/edittimedialog.cpp:71
#, kde-format
msgctxt "@title:group"
msgid "Time Editing"
msgstr ""

#: dialogs/edittimedialog.cpp:79
#, kde-format
msgid "Current Time:"
msgstr ""

#: dialogs/edittimedialog.cpp:82
#, fuzzy, kde-format
msgctxt "@item:valuesuffix Change Time By: ... minute(s)"
msgid " minute"
msgid_plural " minutes"
msgstr[0] " mínútur"
msgstr[1] " mínútur"

#: dialogs/edittimedialog.cpp:88
#, kde-format
msgid "Change Time By:"
msgstr ""

#: dialogs/edittimedialog.cpp:92
#, kde-format
msgid "Time After Change:"
msgstr ""

#: dialogs/edittimedialog.cpp:98
#, fuzzy, kde-format
#| msgid "Log history"
msgctxt "@action:button"
msgid "Edit History..."
msgstr "Annálasaga"

#: dialogs/edittimedialog.cpp:99
#, kde-format
msgid "To change this task's time, you have to edit its event history"
msgstr ""

#: dialogs/exportdialog.cpp:96
#, fuzzy, kde-format
msgctxt "@title:window"
msgid "Export to File"
msgstr "&Flytja út í CSV skrá..."

#. i18n: ectx: property (text), item, widget (QComboBox, combodecimalminutes)
#: dialogs/exportdialog.cpp:117 dialogs/exportdialog.ui:177
#, fuzzy, kde-format
msgctxt "format to display times"
msgid "Decimal"
msgstr "Komma"

#. i18n: ectx: property (text), item, widget (QComboBox, combosessiontimes)
#: dialogs/exportdialog.cpp:137 dialogs/exportdialog.ui:196
#, fuzzy, kde-format
msgid "Session Times"
msgstr "Lengd setu"

#. i18n: ectx: property (text), item, widget (QComboBox, comboalltasks)
#: dialogs/exportdialog.cpp:138 dialogs/exportdialog.ui:205
#, fuzzy, kde-format
msgid "All Tasks"
msgstr "Afrita öll verkefni"

#. i18n: ectx: property (windowTitle), widget (QDialog, ExportDialog)
#: dialogs/exportdialog.ui:14
#, fuzzy, kde-format
#| msgid "&Export"
msgid "Export"
msgstr "Flytja ú&t"

#. i18n: ectx: property (title), widget (QGroupBox, grpPreview)
#: dialogs/exportdialog.ui:26
#, fuzzy, kde-format
msgid "Export Preview"
msgstr "&Flytja út í CSV skrá..."

#. i18n: ectx: property (text), widget (QPushButton, btnToClipboard)
#: dialogs/exportdialog.ui:50
#, fuzzy, kde-format
msgid "Copy to Clipboard"
msgstr "&Afrita heildartíma á klippiborð"

#. i18n: ectx: property (text), widget (QPushButton, btnSaveAs)
#: dialogs/exportdialog.ui:60
#, kde-format
msgid "Save As..."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, grpReportType)
#: dialogs/exportdialog.ui:75
#, kde-format
msgid "Report Type"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, radioTimesCsv)
#: dialogs/exportdialog.ui:81
#, kde-format
msgid "Times as CSV"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, radioHistoryCsv)
#: dialogs/exportdialog.ui:91
#, kde-format
msgid "History as CSV"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, radioEventLogCsv)
#: dialogs/exportdialog.ui:98
#, fuzzy, kde-format
#| msgid "Export to:"
msgid "Event Log as CSV"
msgstr "Flytja út í:"

#. i18n: ectx: property (text), widget (QRadioButton, radioTimesText)
#: dialogs/exportdialog.ui:105
#, kde-format
msgid "Times as Text"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QGroupBox, grpDateRange)
#: dialogs/exportdialog.ui:118
#, kde-format
msgid ""
"An inclusive date range for reporting on time card history.  Not enabled "
"when reporting on totals."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, grpDateRange)
#: dialogs/exportdialog.ui:121
#, kde-format
msgid "Date Range"
msgstr "Tímabil"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#: dialogs/exportdialog.ui:127
#, kde-format
msgid "From:"
msgstr "Frá:"

#. i18n: ectx: property (text), widget (QLabel, textLabel1_2)
#: dialogs/exportdialog.ui:150
#, kde-format
msgid "To:"
msgstr "Til:"

#. i18n: ectx: property (text), item, widget (QComboBox, combodecimalminutes)
#: dialogs/exportdialog.ui:182
#, kde-format
msgid "Hours:Minutes"
msgstr "Klukkustundir:mínútur"

#. i18n: ectx: property (text), item, widget (QComboBox, combosessiontimes)
#: dialogs/exportdialog.ui:191
#, fuzzy, kde-format
msgid "All Times"
msgstr "Endurstilla alla tíma"

#. i18n: ectx: property (text), item, widget (QComboBox, comboalltasks)
#: dialogs/exportdialog.ui:210
#, fuzzy, kde-format
msgid "Only Selected"
msgstr "Eyði verkefni"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, grpDelimiter)
#: dialogs/exportdialog.ui:224
#, kde-format
msgid "The character used to separate one field from another in the output."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, grpDelimiter)
#: dialogs/exportdialog.ui:227
#, kde-format
msgid "Delimiter"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, radioComma)
#: dialogs/exportdialog.ui:233
#, kde-format
msgid "Comma"
msgstr "Komma"

#. i18n: ectx: property (text), widget (QRadioButton, radioSemicolon)
#: dialogs/exportdialog.ui:243
#, kde-format
msgid "Semicolon"
msgstr "Semikomma"

#. i18n: ectx: property (text), widget (QRadioButton, radioOther)
#: dialogs/exportdialog.ui:250
#, fuzzy, kde-format
#| msgid "Other:"
msgctxt "user can set an user defined delimiter"
msgid "Other:"
msgstr "Annað:"

#. i18n: ectx: property (text), widget (QRadioButton, radioTab)
#: dialogs/exportdialog.ui:257
#, fuzzy, kde-format
#| msgid "Tab"
msgctxt "tabulator delimiter"
msgid "Tab"
msgstr "Tab"

#. i18n: ectx: property (text), widget (QRadioButton, radioSpace)
#: dialogs/exportdialog.ui:264
#, kde-format
msgid "Space"
msgstr "Bil"

#. i18n: ectx: property (text), widget (QLabel, quotesLabel)
#: dialogs/exportdialog.ui:308
#, kde-format
msgid "Quotes:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QComboBox, cboQuote)
#: dialogs/exportdialog.ui:327
#, kde-format
msgid "All fields are quoted in the output."
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, cboQuote)
#: dialogs/exportdialog.ui:331
#, kde-format
msgid "\""
msgstr "\""

#. i18n: ectx: property (text), item, widget (QComboBox, cboQuote)
#: dialogs/exportdialog.ui:336
#, kde-format
msgid "'"
msgstr "'"

#: dialogs/historydialog.cpp:105 export/totalsastext.cpp:87
#, kde-format
msgid "Task"
msgstr "Verkefni"

#: dialogs/historydialog.cpp:105
#, fuzzy, kde-format
#| msgid "Total Time"
msgid "StartTime"
msgstr "Heildartími"

#: dialogs/historydialog.cpp:105
#, fuzzy, kde-format
#| msgid "Time"
msgid "EndTime"
msgstr "Tími"

#: dialogs/historydialog.cpp:105
#, fuzzy, kde-format
#| msgid "Comma"
msgid "Comment"
msgstr "Komma"

#: dialogs/historydialog.cpp:146
#, kde-format
msgctxt "@info:whatsthis"
msgid "You can change this task's comment, start time and end time."
msgstr ""

#: dialogs/historydialog.cpp:197 dialogs/historydialog.cpp:217
#, kde-format
msgid "This is not a valid Date/Time."
msgstr ""

#: dialogs/historydialog.cpp:273
#, kde-format
msgid "Please select a task to delete."
msgstr ""

#. i18n: ectx: property (windowTitle), widget (QDialog, HistoryDialog)
#: dialogs/historydialog.ui:14
#, fuzzy, kde-format
#| msgid "Log history"
msgid "Edit History"
msgstr "Annálasaga"

#: dialogs/taskpropertiesdialog.cpp:62
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"<p>Enter the name of the task here. You can choose it freely.</p>\n"
"<p><i>Example:</i> phone with mother</p>"
msgstr ""

#: dialogs/taskpropertiesdialog.cpp:73
#, fuzzy, kde-format
#| msgid "A&uto tracking"
msgctxt "@title:group"
msgid "Auto Tracking"
msgstr "Sjálfvirk tí&mataka"

#: dialogs/taskpropertiesdialog.cpp:90
#, kde-format
msgctxt "order number of desktop: 1, 2, ..."
msgid "%1."
msgstr ""

#: export/export.cpp:63
#, kde-format
msgid "Could not open \"%1\"."
msgstr "Gat ekki opnað \"%1\"."

#: export/totalsastext.cpp:84
#, kde-format
msgid "Task Totals"
msgstr "Samtals"

#: export/totalsastext.cpp:87
#, kde-format
msgid "Time"
msgstr "Tími"

#: export/totalsastext.cpp:120
#, fuzzy, kde-format
#| msgid "Total"
msgctxt "total time of all tasks"
msgid "Total"
msgstr "Samtals"

#: export/totalsastext.cpp:122
#, kde-format
msgid "No tasks."
msgstr "Engin verkefni."

#: idletimedetector.cpp:78
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Apply the idle time since %1 to all active\n"
"timers and keep them running."
msgstr ""

#: idletimedetector.cpp:81
#, fuzzy, kde-format
msgctxt "@action:button"
msgid "Continue Timing"
msgstr "Mæla tíma áfram"

#: idletimedetector.cpp:83
#, kde-format
msgctxt "@info:tooltip"
msgid "Stop timing and revert back to the time at %1"
msgstr ""

#: idletimedetector.cpp:84
#, fuzzy, kde-format
msgctxt "@action:button"
msgid "Revert Timing"
msgstr "Bakka og halda áfram"

#: idletimedetector.cpp:88
#, kde-format
msgid "Desktop has been idle since %1. What do you want to do?"
msgstr ""

#. i18n: ectx: Menu (clock)
#: ktimetrackerui.rc:19
#, kde-format
msgid "&Clock"
msgstr "&Klukka"

#. i18n: ectx: Menu (task)
#: ktimetrackerui.rc:26
#, kde-format
msgid "&Task"
msgstr "&Verkefni"

#. i18n: ectx: Menu (settings)
#: ktimetrackerui.rc:36
#, fuzzy, kde-format
msgid "&Settings"
msgstr "Stilla birtingu"

#. i18n: ectx: ToolBar (mainToolBar)
#: ktimetrackerui.rc:41
#, kde-format
msgid "Main Toolbar"
msgstr ""

#. i18n: ectx: ToolBar (taskToolBar)
#: ktimetrackerui.rc:50
#, fuzzy, kde-format
#| msgid "Task"
msgid "Tasks"
msgstr "Verkefni"

#: main.cpp:100 model/event.cpp:167 model/eventsmodel.cpp:126
#, fuzzy, kde-format
msgid "KTimeTracker"
msgstr "KDE tímarakningartól"

#: main.cpp:102
#, kde-format
msgid "KDE Time tracker tool"
msgstr "KDE tímarakningartól"

#: main.cpp:104
#, kde-format
msgid "Copyright © 1997-2019 KTimeTracker developers"
msgstr ""

#: main.cpp:108
#, kde-format
msgctxt "@info:credit"
msgid "Alexander Potashev"
msgstr ""

#: main.cpp:109
#, fuzzy, kde-format
#| msgid "Current Maintainer"
msgctxt "@info:credit"
msgid "Current Maintainer (since 2019)"
msgstr "Núverandi umsjónaraðili"

#: main.cpp:111
#, kde-format
msgctxt "@info:credit"
msgid "Thorsten Stärk"
msgstr ""

#: main.cpp:112
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer (2006-2012)"
msgstr ""

#: main.cpp:114
#, kde-format
msgctxt "@info:credit"
msgid "Mark Bucciarelli"
msgstr ""

#: main.cpp:115
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer (2005-2006)"
msgstr ""

#: main.cpp:117
#, kde-format
msgctxt "@info:credit"
msgid "Jesper Pedersen"
msgstr ""

#: main.cpp:118
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer (2000-2005)"
msgstr ""

#: main.cpp:120
#, kde-format
msgctxt "@info:credit"
msgid "Sirtaj Singh Kang"
msgstr ""

#: main.cpp:121
#, fuzzy, kde-format
#| msgid "Original Author"
msgctxt "@info:credit"
msgid "Original Author"
msgstr "Upprunalegur höfundur"

#: main.cpp:123
#, kde-format
msgctxt "@info:credit"
msgid "Mathias Soeken"
msgstr ""

#: main.cpp:124
#, kde-format
msgctxt "@info:credit"
msgid "Developer (in 2007)"
msgstr ""

#: main.cpp:126
#, kde-format
msgctxt "@info:credit"
msgid "Kalle Dalheimer"
msgstr ""

#: main.cpp:127
#, kde-format
msgctxt "@info:credit"
msgid "Developer (1999-2000)"
msgstr ""

#: main.cpp:129
#, kde-format
msgctxt "@info:credit"
msgid "Allen Winter"
msgstr ""

#: main.cpp:130 main.cpp:133
#, kde-format
msgctxt "@info:credit"
msgid "Developer"
msgstr ""

#: main.cpp:132
#, kde-format
msgctxt "@info:credit"
msgid "David Faure"
msgstr ""

#: main.cpp:144
#, fuzzy, kde-format
msgctxt "@info:shell"
msgid "Path or URL to iCalendar file to open."
msgstr "iCalendar skrá:"

#: mainwindow.cpp:52
#, fuzzy, kde-format
msgctxt "@action:inmenu"
msgid "Configure KTimeTracker..."
msgstr "KDE tímarakningartól"

#: mainwindow.cpp:152 tray.cpp:90
#, kde-format
msgid "No active tasks"
msgstr "Engin virk verkefni"

#: mainwindow.cpp:162 tray.cpp:110
#, fuzzy, kde-format
#| msgid ", "
msgctxt "separator between task names"
msgid ", "
msgstr ", "

#: model/tasksmodel.cpp:33
#, fuzzy, kde-format
#| msgid "Task Name"
msgctxt "@title:column"
msgid "Task Name"
msgstr "Nafn verkefnis"

#: model/tasksmodel.cpp:34
#, fuzzy, kde-format
#| msgid "Session Time"
msgctxt "@title:column"
msgid "Session Time"
msgstr "Lengd setu"

#: model/tasksmodel.cpp:35
#, fuzzy, kde-format
#| msgid "Time"
msgctxt "@title:column"
msgid "Time"
msgstr "Tími"

#: model/tasksmodel.cpp:36
#, fuzzy, kde-format
#| msgid "Total Session Time"
msgctxt "@title:column"
msgid "Total Session Time"
msgstr "Heildarlengd setu"

#: model/tasksmodel.cpp:37
#, fuzzy, kde-format
#| msgid "Total Time"
msgctxt "@title:column"
msgid "Total Time"
msgstr "Heildartími"

#: model/tasksmodel.cpp:38
#, kde-format
msgctxt "@title:column"
msgid "Priority"
msgstr ""

#: model/tasksmodel.cpp:39
#, fuzzy, kde-format
msgctxt "@title:column"
msgid "Percent Complete"
msgstr "&Merkja sem lokið"

#: model/tasksmodel.cpp:189 model/tasksmodel.cpp:282
#, fuzzy, kde-format
#| msgid "Enter the name of the task here. This name is for your eyes only."
msgctxt "@info:whatsthis"
msgid "The task name is what you call the task, it can be chosen freely."
msgstr ""
"Sláðu inn nafn verkefnisins hér.  Þetta er nafnið sem aðeins þú munt sjá."

#: model/tasksmodel.cpp:193 model/tasksmodel.cpp:286
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The session time is the time since you last chose \"Start New Session\"."
msgstr ""

#: model/tasksmodel.cpp:290
#, fuzzy, kde-format
#| msgid "This will delete the selected task and all its subtasks."
msgctxt "@info:whatsthis"
msgid ""
"The total session time is the session time of this task and all its subtasks."
msgstr "Þetta mun eyða völdu verkefni og öllum undirverkefnum þess"

#: model/tasksmodel.cpp:294
#, fuzzy, kde-format
#| msgid "This will delete the selected task and all its subtasks."
msgctxt "@info:whatsthis"
msgid "The total time is the time of this task and all its subtasks."
msgstr "Þetta mun eyða völdu verkefni og öllum undirverkefnum þess"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_enabled)
#: settings/cfgbehavior.ui:17
#, fuzzy, kde-format
#| msgid "Detect desktop as idle after"
msgid "Detect desktop as idle after:"
msgstr "Skynja skjáborð sem óvirkt eftir"

#. i18n: ectx: property (suffix), widget (QSpinBox, kcfg_autoSavePeriod)
#. i18n: ectx: property (suffix), widget (QSpinBox, kcfg_period)
#: settings/cfgbehavior.ui:24 settings/cfgstorage.ui:36
#, fuzzy, kde-format
msgid " min"
msgstr " mínútur"

#. i18n: ectx: property (text), widget (QLabel, label)
#: settings/cfgbehavior.ui:37
#, kde-format
msgid "Minimum desktop active time:"
msgstr ""

#. i18n: ectx: property (suffix), widget (QSpinBox, kcfg_minActiveTime)
#: settings/cfgbehavior.ui:44
#, kde-format
msgid " sec"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_promptDelete)
#: settings/cfgbehavior.ui:57
#, kde-format
msgid "Prompt before deleting tasks"
msgstr "Spurja áður en verkefnum er eytt"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, kcfg_uniTasking)
#: settings/cfgbehavior.ui:64
#, kde-format
msgid ""
"Unitasking - allow only one task to be timed at a time. Does not stop any "
"timer."
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_uniTasking)
#: settings/cfgbehavior.ui:67
#, kde-format
msgid "Allow only one timer at a time"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_trayIcon)
#: settings/cfgbehavior.ui:74
#, kde-format
msgid "Place an icon to the system tray"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: settings/cfgdisplay.ui:29
#, kde-format
msgctxt "title of group box, general options"
msgid "General"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_decimalFormat)
#: settings/cfgdisplay.ui:35
#, fuzzy, kde-format
msgid "Decimal number format"
msgstr "Tímaform"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, kcfg_configPDA)
#: settings/cfgdisplay.ui:42
#, kde-format
msgid ""
"Choose this if you have a touchscreen and your screen real estate is "
"precious. It will disable the search bar and every click will pop up a "
"context menu."
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_configPDA)
#: settings/cfgdisplay.ui:45
#, fuzzy, kde-format
#| msgid "Confirmation Required"
msgctxt ""
"Choose this if you have a touchscreen and your screen real estate is "
"precious."
msgid "Configuration for PDA"
msgstr "Staðfestingar er krafist"

#. i18n: ectx: property (title), widget (QGroupBox, windowTitleGroupBox)
#: settings/cfgdisplay.ui:55
#, kde-format
msgid "Window Title"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_windowTitleCurrentFile)
#: settings/cfgdisplay.ui:64
#, kde-format
msgid "Currently opened file"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_windowTitleCurrentTask)
#: settings/cfgdisplay.ui:71
#, fuzzy, kde-format
#| msgid "Total Session Time"
msgid "Current Task and Session Time"
msgstr "Heildarlengd setu"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: settings/cfgdisplay.ui:81
#, fuzzy, kde-format
#| msgid "Columns displayed:"
msgid "Columns Displayed"
msgstr "Sýna dálka:"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_displaySessionTime)
#: settings/cfgdisplay.ui:87
#, kde-format
msgid "Session time"
msgstr "Lengd &setu:"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_displayTime)
#: settings/cfgdisplay.ui:94
#, kde-format
msgid "Cumulative task time"
msgstr "Heildartími verkefnis"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_displayTotalSessionTime)
#: settings/cfgdisplay.ui:101
#, kde-format
msgid "Total session time"
msgstr "Heildar setutíma"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_displayTotalTime)
#: settings/cfgdisplay.ui:108
#, kde-format
msgid "Total task time"
msgstr "Heildartíma verkefna"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_displayPriority)
#: settings/cfgdisplay.ui:115
#, kde-format
msgid "Priority"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_displayPercentComplete)
#: settings/cfgdisplay.ui:122
#, fuzzy, kde-format
msgid "Percent complete"
msgstr "&Merkja sem lokið"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_autoSave)
#: settings/cfgstorage.ui:29
#, fuzzy, kde-format
#| msgid "Save tasks every"
msgid "Save tasks every:"
msgstr "Vista verkefni hverjar"

#: taskview.cpp:118
#, kde-format
msgid ""
"Error storing new task. Your changes were not saved. Make sure you can edit "
"your iCalendar file. Also quit all applications using this file and remove "
"any lock file related to its name from ~/.kde/share/apps/kabc/lock/ "
msgstr ""

#: taskview.cpp:214
#, kde-format
msgid ""
"Your virtual desktop number is too high, desktop tracking will not work."
msgstr ""

#: taskview.cpp:290
#, kde-format
msgctxt "@info:progress"
msgid "Stopping timers..."
msgstr ""

#: taskview.cpp:291
#, kde-format
msgid "Cancel"
msgstr ""

#: taskview.cpp:361 taskview.cpp:445
#, kde-format
msgid "Unnamed Task"
msgstr "Ónefnt verkefni"

#: taskview.cpp:380
#, kde-format
msgid ""
"Error storing new task. Your changes were not saved. Make sure you can edit "
"your iCalendar file. Also quit all applications using this file and remove "
"any lock file related to its name from ~/.kde/share/apps/kabc/lock/"
msgstr ""

#: taskview.cpp:422
#, fuzzy, kde-format
#| msgid "New Sub Task"
msgctxt "@title:window"
msgid "New Sub Task"
msgstr "Nýtt undirverkefni"

#: taskview.cpp:440
#, fuzzy, kde-format
#| msgid "Edit Task"
msgctxt "@title:window"
msgid "Edit Task"
msgstr "Breyta verki"

#: taskview.cpp:474 taskview.cpp:515 taskview.cpp:537
#, kde-format
msgid "No task selected."
msgstr "Ekkert verkefni valið."

#: taskview.cpp:520
#, fuzzy, kde-format
#| msgid ""
#| "Are you sure you want to delete the task named\n"
#| "\"%1\" and its entire history?\n"
#| "NOTE: all its subtasks and their history will also be deleted."
msgid ""
"Are you sure you want to delete the selected task and its entire history?\n"
"Note: All subtasks and their history will also be deleted."
msgstr ""
"Ertu viss um að þú viljir eyða verkefninu\n"
"\"%1\" og allri sögu þess?\n"
"ATH: öllum undirverkefnum mun einnig verða eytt!"

#: taskview.cpp:524
#, fuzzy, kde-format
#| msgid "Deleting Task"
msgctxt "@title:window"
msgid "Deleting Task"
msgstr "Eyði verkefni"

#: timetrackerstorage.cpp:140
#, kde-format
msgid "Error loading \"%1\": could not find parent (uid=%2)"
msgstr "Gat ekki opnað \"%1\": fann ekki foreldri (uid=%2)"

#: timetrackerstorage.cpp:243
#, fuzzy, kde-format
#| msgid "Could not open \"%1\"."
msgctxt "%1=lock file path"
msgid "Could not write lock file \"%1\". Disk full?"
msgstr "Gat ekki opnað \"%1\"."

#: timetrackerstorage.cpp:251
#, kde-format
msgctxt "%1=destination file path/URL"
msgid "Failed to save iCalendar file as \"%1\"."
msgstr ""

#: timetrackerwidget.cpp:154
#, fuzzy, kde-format
#| msgid "Start &New Session"
msgctxt "@action:inmenu"
msgid "Start &New Session"
msgstr "Hefja &nýjan setutíma"

#: timetrackerwidget.cpp:155
#, fuzzy, kde-format
#| msgid "Start a new session"
msgctxt "@info:tooltip"
msgid "Starts a new session"
msgstr "Hefja nýjan setutíma"

#: timetrackerwidget.cpp:157
#, fuzzy, kde-format
#| msgid ""
#| "This will reset the session time to 0 for all tasks, to start a new "
#| "session, without affecting the totals."
msgctxt "@info:whatsthis"
msgid ""
"This will reset the session time to 0 for all tasks, to start a new session, "
"without affecting the totals."
msgstr ""
"Þetta endurstillir setutíma allra verkefna á 0. Til að hefja nýjan setutíma, "
"án þess að hafa áhrif á heildartíma"

#: timetrackerwidget.cpp:165
#, fuzzy, kde-format
#| msgid "Log history"
msgctxt "@action:inmenu"
msgid "Edit History..."
msgstr "Annálasaga"

#: timetrackerwidget.cpp:166
#, kde-format
msgctxt "@info:tooltip"
msgid "Edits history of all tasks of the current document"
msgstr ""

#: timetrackerwidget.cpp:168
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"A window will be opened where you can change start and stop times of tasks "
"or add a comment to them."
msgstr ""

#: timetrackerwidget.cpp:178
#, fuzzy, kde-format
#| msgid "&Reset All Times"
msgctxt "@action:inmenu"
msgid "&Reset All Times"
msgstr "Endu&rstilla allar tímatökur"

#: timetrackerwidget.cpp:179
#, fuzzy, kde-format
#| msgid "Reset all times"
msgctxt "@info:tooltip"
msgid "Resets all times"
msgstr "Endurstilla alla tíma"

#: timetrackerwidget.cpp:181
#, fuzzy, kde-format
#| msgid ""
#| "This will reset the session and total time to 0 for all tasks, to restart "
#| "from scratch."
msgctxt "@info:whatsthis"
msgid ""
"This will reset the session and total time to 0 for all tasks, to restart "
"from scratch."
msgstr ""
"Þetta endurstillir setutíma allra verkefna auk heildartíma á 0 svo að hægt "
"sé að byrja upp á nýtt."

#: timetrackerwidget.cpp:188
#, fuzzy, kde-format
#| msgid "Total Time"
msgctxt "@action:inmenu"
msgid "&Start"
msgstr "Heildartími"

#: timetrackerwidget.cpp:189
#, fuzzy, kde-format
#| msgid "Start timing for selected task"
msgctxt "@info:tooltip"
msgid "Starts timing for selected task"
msgstr "Ræsa tímatöku fyrir valið verkefni"

#: timetrackerwidget.cpp:191
#, fuzzy, kde-format
#| msgid ""
#| "This will start timing for the selected task.\n"
#| "It is even possible to time several tasks simultaneously.\n"
#| "\n"
#| "You may also start timing of a tasks by double clicking the left mouse "
#| "button on a given task. This will, however, stop timing of other tasks."
msgctxt "@info:whatsthis"
msgid ""
"This will start timing for the selected task.\n"
"It is even possible to time several tasks simultaneously.\n"
"\n"
"You may also start timing of tasks by double clicking the left mouse button "
"on a given task. This will, however, stop timing of other tasks."
msgstr ""
"Þetta ræsir tímatöku fyrir valið verkefni.\n"
"Það er jafnvel mögulegt að ræsa nokkur verkefni í einu.\n"
"\n"
"Það er einnig hægt að ræsa tímatöku fyrir verkefni með því að tvísmella með "
"vinstri músarhnappi á verkefnið. Þetta mun aftur á móti stöðva tímatöku "
"fyrir önnur verkefni."

#: timetrackerwidget.cpp:205
#, fuzzy, kde-format
#| msgid "S&top"
msgctxt "@action:inmenu"
msgid "S&top"
msgstr "S&töðva"

#: timetrackerwidget.cpp:206
#, fuzzy, kde-format
#| msgid "Stop timing of the selected task"
msgctxt "@info:tooltip"
msgid "Stops timing of the selected task"
msgstr "Stöðva tímatöku fyrir valið verkefni"

#: timetrackerwidget.cpp:207
#, fuzzy, kde-format
#| msgid "Stop timing of the selected task"
msgctxt "@info:whatsthis"
msgid "Stops timing of the selected task"
msgstr "Stöðva tímatöku fyrir valið verkefni"

#: timetrackerwidget.cpp:214
#, kde-format
msgctxt "@action:inmenu"
msgid "Focus on Searchbar"
msgstr ""

#: timetrackerwidget.cpp:215
#, kde-format
msgctxt "@info:tooltip"
msgid "Sets the focus on the searchbar"
msgstr ""

#: timetrackerwidget.cpp:216
#, kde-format
msgctxt "@info:whatsthis"
msgid "Sets the focus on the searchbar"
msgstr ""

#: timetrackerwidget.cpp:223
#, fuzzy, kde-format
#| msgid "Stop &All Timers"
msgctxt "@action:inmenu"
msgid "Stop &All Timers"
msgstr "Stöðva &allar tímatökur"

#: timetrackerwidget.cpp:224
#, fuzzy, kde-format
#| msgid "Stop all of the active timers"
msgctxt "@info:tooltip"
msgid "Stops all of the active timers"
msgstr "Stöðva alla virka tímatöku"

#: timetrackerwidget.cpp:225
#, fuzzy, kde-format
#| msgid "Stop all of the active timers"
msgctxt "@info:whatsthis"
msgid "Stops all of the active timers"
msgstr "Stöðva alla virka tímatöku"

#: timetrackerwidget.cpp:233
#, kde-format
msgctxt "@action:inmenu"
msgid "Track Active Applications"
msgstr ""

#: timetrackerwidget.cpp:235
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Auto-creates and updates tasks when the focus of the current window has "
"changed"
msgstr ""

#: timetrackerwidget.cpp:238
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"If the focus of a window changes for the first time when this action is "
"enabled, a new task will be created with the title of the window as its name "
"and will be started. If there already exists such an task it will be started."
msgstr ""

#: timetrackerwidget.cpp:249
#, fuzzy, kde-format
#| msgid "New Task"
msgctxt "@action:inmenu"
msgid "&New Task..."
msgstr "Nýtt verkefni"

#: timetrackerwidget.cpp:250
#, fuzzy, kde-format
#| msgid "Create new top level task"
msgctxt "@info:tooltip"
msgid "Creates new top level task"
msgstr "Búa til nýtt verkefni í efsta þrepi"

#: timetrackerwidget.cpp:251
#, fuzzy, kde-format
#| msgid "This will create a new top level task."
msgctxt "@info:whatsthis"
msgid "This will create a new top level task."
msgstr "Þetta býr til nýtt verkefni í efsta þrepi"

#: timetrackerwidget.cpp:259
#, fuzzy, kde-format
#| msgid "New &Subtask..."
msgctxt "@action:inmenu"
msgid "New &Subtask..."
msgstr "Nýtt &undirverkefni"

#: timetrackerwidget.cpp:260
#, fuzzy, kde-format
#| msgid "This will create a new top level task."
msgctxt "@info:tooltip"
msgid "Creates a new subtask to the current selected task"
msgstr "Þetta býr til nýtt verkefni í efsta þrepi"

#: timetrackerwidget.cpp:261
#, fuzzy, kde-format
#| msgid "This will create a new top level task."
msgctxt "@info:whatsthis"
msgid "This will create a new subtask to the current selected task."
msgstr "Þetta býr til nýtt verkefni í efsta þrepi"

#: timetrackerwidget.cpp:269
#, kde-format
msgctxt "@action:inmenu"
msgid "&Delete"
msgstr ""

#: timetrackerwidget.cpp:270
#, fuzzy, kde-format
#| msgid "Delete selected task"
msgctxt "@info:tooltip"
msgid "Deletes selected task"
msgstr "Eyða völdu verkefni"

#: timetrackerwidget.cpp:271
#, fuzzy, kde-format
#| msgid "This will delete the selected task and all its subtasks."
msgctxt "@info:whatsthis"
msgid "This will delete the selected task(s) and all subtasks."
msgstr "Þetta mun eyða völdu verkefni og öllum undirverkefnum þess"

#: timetrackerwidget.cpp:279
#, kde-format
msgctxt "@action:inmenu"
msgid "&Properties"
msgstr ""

#: timetrackerwidget.cpp:280
#, fuzzy, kde-format
#| msgid "Edit name or times for selected task"
msgctxt "@info:tooltip"
msgid "Edit name or description for selected task"
msgstr "Breyta nafni eða tímum verkefnisins"

#: timetrackerwidget.cpp:282
#, fuzzy, kde-format
#| msgid ""
#| "This will bring up a dialog box where you may edit the parameters for the "
#| "selected task."
msgctxt "@info:whatsthis"
msgid ""
"This will bring up a dialog box where you may edit the parameters for the "
"selected task."
msgstr ""
"Við þetta kemur upp gluggi þar sem þú getur breytt stillingum fyrir valið "
"verkefni."

#: timetrackerwidget.cpp:291
#, fuzzy, kde-format
#| msgid "Edit Task"
msgctxt "@action:inmenu"
msgid "Edit &Time..."
msgstr "Breyta verki"

#: timetrackerwidget.cpp:292
#, fuzzy, kde-format
#| msgid "Edit name or times for selected task"
msgctxt "@info:tooltip"
msgid "Edit time for selected task"
msgstr "Breyta nafni eða tímum verkefnisins"

#: timetrackerwidget.cpp:294
#, fuzzy, kde-format
#| msgid ""
#| "This will bring up a dialog box where you may edit the parameters for the "
#| "selected task."
msgctxt "@info:whatsthis"
msgid ""
"This will bring up a dialog box where you may edit the times for the "
"selected task."
msgstr ""
"Við þetta kemur upp gluggi þar sem þú getur breytt stillingum fyrir valið "
"verkefni."

#: timetrackerwidget.cpp:303
#, fuzzy, kde-format
#| msgid "&Mark as Complete"
msgctxt "@action:inmenu"
msgid "&Mark as Complete"
msgstr "&Merkja sem lokið"

#: timetrackerwidget.cpp:311
#, fuzzy, kde-format
msgctxt "@action:inmenu"
msgid "&Mark as Incomplete"
msgstr "&Merkja sem lokið"

#: timetrackerwidget.cpp:319
#, fuzzy, kde-format
#| msgid "&Export"
msgctxt "@action:inmenu"
msgid "&Export..."
msgstr "Flytja ú&t"

#: timetrackerwidget.cpp:326
#, kde-format
msgctxt "@action:inmenu"
msgid "Import Tasks From &Planner..."
msgstr ""

#: timetrackerwidget.cpp:338
#, kde-format
msgctxt "@action:inmenu"
msgid "Show Searchbar"
msgstr ""

#: timetrackerwidget.cpp:471
#, kde-format
msgctxt "@title:tab"
msgid "Behavior"
msgstr ""

#: timetrackerwidget.cpp:474
#, kde-format
msgctxt "@title:tab"
msgid "Appearance"
msgstr ""

#: timetrackerwidget.cpp:477
#, kde-format
msgctxt "@title:tab"
msgid "Storage"
msgstr ""

#: timetrackerwidget.cpp:514
#, fuzzy, kde-format
#| msgid "New Task"
msgctxt "@title:window"
msgid "New Task"
msgstr "Nýtt verkefni"

#: timetrackerwidget.cpp:593
#, kde-format
msgctxt "@info in message box"
msgid ""
"There is no history yet. Start and stop a task and you will have an entry in "
"your history."
msgstr ""

#: timetrackerwidget.cpp:603
#, kde-format
msgid ""
"Do you really want to reset the time to zero for all tasks? This will delete "
"the entire history."
msgstr ""

#: timetrackerwidget.cpp:605
#, fuzzy, kde-format
#| msgid "Confirmation Required"
msgctxt "@title:window"
msgid "Confirmation Required"
msgstr "Staðfestingar er krafist"

#: timetrackerwidget.cpp:606
#, fuzzy, kde-format
#| msgid "Reset All Times"
msgctxt "@action:button"
msgid "Reset All Times"
msgstr "Endurstilla alla tíma"

#: timetrackerwidget.cpp:795
#, kde-format
msgid "Save failed, most likely because the file could not be locked."
msgstr ""

#: timetrackerwidget.cpp:797
#, kde-format
msgid "Could not modify calendar resource."
msgstr ""

#: timetrackerwidget.cpp:799
#, kde-format
msgid "Out of memory--could not create object."
msgstr ""

#: timetrackerwidget.cpp:801
#, fuzzy, kde-format
msgid "UID not found."
msgstr "Skrá \"%1\" fannst ekki."

#: timetrackerwidget.cpp:803
#, kde-format
msgid "Invalidate date--format is YYYY-MM-DD."
msgstr ""

#: timetrackerwidget.cpp:805
#, kde-format
msgid "Invalid time--format is YYYY-MM-DDTHH:MM:SS."
msgstr ""

#: timetrackerwidget.cpp:807
#, kde-format
msgid "Invalid task duration--must be greater than zero."
msgstr ""

#: timetrackerwidget.cpp:809
#, kde-format
msgid "Invalid error number: %1"
msgstr ""

#: timetrackerwidget.cpp:1038
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"This is ktimetracker, KDE's program to help you track your time. Best, start "
"with creating your first task - enter it into the field where you see "
"\"Search or add task\"."
msgstr ""

#: timetrackerwidget.cpp:1045
#, kde-format
msgctxt "@info:whatsthis"
msgid "You have already created a task. You can now start and stop timing."
msgstr ""

#: tray.cpp:95
#, fuzzy, kde-format
#| msgid ", ..."
msgctxt "ellipsis to truncate long list of tasks"
msgid ", ..."
msgstr ", ..."

#: treeviewheadercontextmenu.cpp:46
#, kde-format
msgctxt "@title:menu"
msgid "Columns"
msgstr ""

#: widgets/searchline.cpp:37
#, kde-format
msgctxt "@info:placeholder"
msgid "Search or add task"
msgstr ""

#: widgets/searchline.cpp:39
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"This is a combined field. As long as you do not press Enter, it acts as a "
"filter. Then, only tasks that match your input are shown. As soon as you "
"press Enter, your input is used as name to create a new task."
msgstr ""

#: widgets/taskswidget.cpp:143
#, kde-format
msgctxt "@item:inmenu Task progress"
msgid "%1 %"
msgstr ""

#: widgets/taskswidget.cpp:154
#, kde-format
msgctxt "@item:inmenu Task priority"
msgid "unspecified"
msgstr ""

#: widgets/taskswidget.cpp:157
#, kde-format
msgctxt "@item:inmenu Task priority"
msgid "1 (highest)"
msgstr ""

#: widgets/taskswidget.cpp:160
#, kde-format
msgctxt "@item:inmenu Task priority"
msgid "5 (medium)"
msgstr ""

#: widgets/taskswidget.cpp:163
#, kde-format
msgctxt "@item:inmenu Task priority"
msgid "9 (lowest)"
msgstr ""

#~ msgid "CSV Export"
#~ msgstr "CSV útflutningur"

#~ msgid "&Edit..."
#~ msgstr "&Breyta"

#, fuzzy
#~| msgid "Edit Task"
#~ msgid "Add/Edit a task"
#~ msgstr "Breyta verki"

#, fuzzy
#~| msgid "Could not open \"%1\"."
#~ msgid "Could not save."
#~ msgstr "Gat ekki opnað \"%1\"."

#, fuzzy
#~| msgid "&Export"
#~ msgid "&Import"
#~ msgstr "Flytja ú&t"

#, fuzzy
#~ msgid "Export &History..."
#~ msgstr "Flytja s&ögu út í CSV skrá..."

#, fuzzy
#~| msgid "The file where Karm will write the data."
#~ msgid "The file where KTimeTracker will write the data."
#~ msgstr "Skráin sem Karm mun skrifa gögnin."

#, fuzzy
#~ msgid "ktimetracker"
#~ msgstr "KDE tímarakningartól"

#~ msgid "Configure key bindings"
#~ msgstr "Stilla tengingu lykla"

#, fuzzy
#~| msgid "This will let you configure keybindings which is specific to karm"
#~ msgid ""
#~ "This will let you configure keybindings which are specific to ktimetracker"
#~ msgstr ""
#~ "Hér getur þú stillt tengingu lykla sem eru sértækar fyrir Karm forritið"

#~ msgid "Just caught a software interrupt."
#~ msgstr "Fékk rofbeiðni"

#, fuzzy
#~| msgid "Delete selected task"
#~ msgid "Delete task <taskid>"
#~ msgstr "Eyða völdu verkefni"

#, fuzzy
#~| msgid "Start timing for selected task"
#~ msgid "Start timer for task <taskid>"
#~ msgstr "Ræsa tímatöku fyrir valið verkefni"

#, fuzzy
#~| msgid "Start timing for selected task"
#~ msgid "Stop timer for task <taskid>"
#~ msgstr "Ræsa tímatöku fyrir valið verkefni"

#~ msgid "task_popup"
#~ msgstr "task_popup"

#, fuzzy
#~| msgid "Space"
#~ msgid "Save"
#~ msgstr "Bil"

#, fuzzy
#~ msgid "Configure ktimetracker"
#~ msgstr "KDE tímarakningartól"

#~ msgid "Week of %1"
#~ msgstr "Vika í %1"

#, fuzzy
#~ msgid "Settings"
#~ msgstr "Stilla birtingu"

#, fuzzy
#~ msgid "Dialog"
#~ msgstr "Komma"

#, fuzzy
#~ msgid "Successfully saved file "
#~ msgstr "Flytja s&ögu út í CSV skrá..."

#~ msgctxt "abbreviation for hours"
#~ msgid " hr. "
#~ msgstr " klst. "

#~ msgctxt "abbreviation for minutes"
#~ msgid " min. "
#~ msgstr " mín. "

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 5"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 4"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 3"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 2"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 1"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 8"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 9"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 6"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 7"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 10"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 12"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 17"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 11"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 16"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 13"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 18"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 14"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 19"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 15"
#~ msgstr "á skjáborði"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 20"
#~ msgstr "á skjáborði"

#~ msgid "Task &name:"
#~ msgstr "&Nafn verkefnis:"

#~ msgid "Edit &absolute"
#~ msgstr "&Breyta tímum"

#~ msgid "&Time:"
#~ msgstr "&Tími:"

#, fuzzy
#~| msgid "&Session time: "
#~ msgid "&Session time:"
#~ msgstr "Lengd &setu: "

#~ msgid "Edit &relative (apply to both time and session time)"
#~ msgstr "Breyta &hlutfallslega (Á bæði við setu- og heildartíma)"

#, fuzzy
#~ msgid ""
#~ "This is the time the task has been running since all times were reset."
#~ msgstr "Þetta er tíminn sem verkefnið hefur verið í vinnslu í þessari setu"

#~ msgid "This is the time the task has been running this session."
#~ msgstr "Þetta er tíminn sem verkefnið hefur verið í vinnslu í þessari setu"

#~ msgid ""
#~ "Specify how much time to add or subtract to the overall and session time"
#~ msgstr ""
#~ "Settu inn tímann sem á að bæta við eða draga frá heildar- og setutímanum"
