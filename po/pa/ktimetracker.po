# translation of karm.po to Punjabi
#
# Amanpreet Singh Alam <aalam@redhat.com>, 2004, 2005.
# ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ <aalam@redhat.com>, 2005.
# A S Alam <aalam@users.sf.net>, 2007, 2010.
msgid ""
msgstr ""
"Project-Id-Version: karm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-29 00:44+0000\n"
"PO-Revision-Date: 2010-01-16 09:20+0530\n"
"Last-Translator: A S Alam <aalam@users.sf.net>\n"
"Language-Team: ਪੰਜਾਬੀ <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "aalam@users.sf.net"

#: dialogs/edittimedialog.cpp:42
#, fuzzy, kde-format
#| msgid "Edit Task"
msgctxt "@title:window"
msgid "Edit Task Time"
msgstr "ਕੰਮ ਸੋਧ"

#: dialogs/edittimedialog.cpp:50 dialogs/taskpropertiesdialog.cpp:56
#, fuzzy, kde-format
#| msgid "Task"
msgctxt "@title:group"
msgid "Task"
msgstr "ਕੰਮ"

#: dialogs/edittimedialog.cpp:61 dialogs/taskpropertiesdialog.cpp:65
#, kde-format
msgid "Task Name:"
msgstr "ਕੰਮ ਨਾਂ:"

#: dialogs/edittimedialog.cpp:68 dialogs/taskpropertiesdialog.cpp:70
#, kde-format
msgid "Task Description:"
msgstr ""

#: dialogs/edittimedialog.cpp:71
#, kde-format
msgctxt "@title:group"
msgid "Time Editing"
msgstr ""

#: dialogs/edittimedialog.cpp:79
#, kde-format
msgid "Current Time:"
msgstr ""

#: dialogs/edittimedialog.cpp:82
#, fuzzy, kde-format
#| msgid " min"
msgctxt "@item:valuesuffix Change Time By: ... minute(s)"
msgid " minute"
msgid_plural " minutes"
msgstr[0] " ਮਿੰਟ"
msgstr[1] " ਮਿੰਟ"

#: dialogs/edittimedialog.cpp:88
#, kde-format
msgid "Change Time By:"
msgstr ""

#: dialogs/edittimedialog.cpp:92
#, kde-format
msgid "Time After Change:"
msgstr ""

#: dialogs/edittimedialog.cpp:98
#, fuzzy, kde-format
#| msgid "Edit History..."
msgctxt "@action:button"
msgid "Edit History..."
msgstr "ਅਤੀਤ ਸੋਧ..."

#: dialogs/edittimedialog.cpp:99
#, kde-format
msgid "To change this task's time, you have to edit its event history"
msgstr ""

#: dialogs/exportdialog.cpp:96
#, fuzzy, kde-format
#| msgid "Export Progress"
msgctxt "@title:window"
msgid "Export to File"
msgstr "ਐਕਸਪੋਰਟ ਤਰੱਕੀ"

#. i18n: ectx: property (text), item, widget (QComboBox, combodecimalminutes)
#: dialogs/exportdialog.cpp:117 dialogs/exportdialog.ui:177
#, kde-format
msgctxt "format to display times"
msgid "Decimal"
msgstr "ਦਸ਼ਮਲਵ"

#. i18n: ectx: property (text), item, widget (QComboBox, combosessiontimes)
#: dialogs/exportdialog.cpp:137 dialogs/exportdialog.ui:196
#, kde-format
msgid "Session Times"
msgstr "ਸ਼ੈਸ਼ਨ ਸਮਾਂ"

#. i18n: ectx: property (text), item, widget (QComboBox, comboalltasks)
#: dialogs/exportdialog.cpp:138 dialogs/exportdialog.ui:205
#, kde-format
msgid "All Tasks"
msgstr "ਸਭ ਕੰਮ"

#. i18n: ectx: property (windowTitle), widget (QDialog, ExportDialog)
#: dialogs/exportdialog.ui:14
#, fuzzy, kde-format
#| msgctxt "@action:button"
#| msgid "&Export"
msgid "Export"
msgstr "ਐਕਸਪੋਰਟ(&E)"

#. i18n: ectx: property (title), widget (QGroupBox, grpPreview)
#: dialogs/exportdialog.ui:26
#, fuzzy, kde-format
#| msgid "Export Progress"
msgid "Export Preview"
msgstr "ਐਕਸਪੋਰਟ ਤਰੱਕੀ"

#. i18n: ectx: property (text), widget (QPushButton, btnToClipboard)
#: dialogs/exportdialog.ui:50
#, fuzzy, kde-format
#| msgctxt "@action:button"
#| msgid "E&xport to Clipboard"
msgid "Copy to Clipboard"
msgstr "ਕਲਿੱਪਬੋਰਡ 'ਚ ਐਕਸਪੋਰਟ(&x)"

#. i18n: ectx: property (text), widget (QPushButton, btnSaveAs)
#: dialogs/exportdialog.ui:60
#, kde-format
msgid "Save As..."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, grpReportType)
#: dialogs/exportdialog.ui:75
#, kde-format
msgid "Report Type"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, radioTimesCsv)
#: dialogs/exportdialog.ui:81
#, kde-format
msgid "Times as CSV"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, radioHistoryCsv)
#: dialogs/exportdialog.ui:91
#, kde-format
msgid "History as CSV"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, radioEventLogCsv)
#: dialogs/exportdialog.ui:98
#, fuzzy, kde-format
#| msgid "Export to:"
msgid "Event Log as CSV"
msgstr "ਐਕਸਪੋਰਟ:"

#. i18n: ectx: property (text), widget (QRadioButton, radioTimesText)
#: dialogs/exportdialog.ui:105
#, kde-format
msgid "Times as Text"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QGroupBox, grpDateRange)
#: dialogs/exportdialog.ui:118
#, kde-format
msgid ""
"An inclusive date range for reporting on time card history.  Not enabled "
"when reporting on totals."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, grpDateRange)
#: dialogs/exportdialog.ui:121
#, kde-format
msgid "Date Range"
msgstr "ਮਿਤੀ ਸੀਮਾ"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#: dialogs/exportdialog.ui:127
#, kde-format
msgid "From:"
msgstr "ਵਲੋਂ:"

#. i18n: ectx: property (text), widget (QLabel, textLabel1_2)
#: dialogs/exportdialog.ui:150
#, kde-format
msgid "To:"
msgstr "ਵੱਲ:"

#. i18n: ectx: property (text), item, widget (QComboBox, combodecimalminutes)
#: dialogs/exportdialog.ui:182
#, kde-format
msgid "Hours:Minutes"
msgstr "ਘੰਟੇ: ਮਿੰਟ"

#. i18n: ectx: property (text), item, widget (QComboBox, combosessiontimes)
#: dialogs/exportdialog.ui:191
#, kde-format
msgid "All Times"
msgstr "ਸਭ ਸਮੇਂ"

#. i18n: ectx: property (text), item, widget (QComboBox, comboalltasks)
#: dialogs/exportdialog.ui:210
#, kde-format
msgid "Only Selected"
msgstr "ਸਿਰਫ਼ ਚੁਣੇ"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, grpDelimiter)
#: dialogs/exportdialog.ui:224
#, kde-format
msgid "The character used to separate one field from another in the output."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, grpDelimiter)
#: dialogs/exportdialog.ui:227
#, kde-format
msgid "Delimiter"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, radioComma)
#: dialogs/exportdialog.ui:233
#, kde-format
msgid "Comma"
msgstr "ਕਾਮਾ"

#. i18n: ectx: property (text), widget (QRadioButton, radioSemicolon)
#: dialogs/exportdialog.ui:243
#, kde-format
msgid "Semicolon"
msgstr "ਅਰਧ-ਵਿਰਾਮ"

#. i18n: ectx: property (text), widget (QRadioButton, radioOther)
#: dialogs/exportdialog.ui:250
#, fuzzy, kde-format
#| msgid "Other:"
msgctxt "user can set an user defined delimiter"
msgid "Other:"
msgstr "ਹੋਰ:"

#. i18n: ectx: property (text), widget (QRadioButton, radioTab)
#: dialogs/exportdialog.ui:257
#, fuzzy, kde-format
#| msgid "Tab"
msgctxt "tabulator delimiter"
msgid "Tab"
msgstr "ਟੈਬ"

#. i18n: ectx: property (text), widget (QRadioButton, radioSpace)
#: dialogs/exportdialog.ui:264
#, kde-format
msgid "Space"
msgstr "ਖਾਲੀ ਥਾਂ"

#. i18n: ectx: property (text), widget (QLabel, quotesLabel)
#: dialogs/exportdialog.ui:308
#, kde-format
msgid "Quotes:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QComboBox, cboQuote)
#: dialogs/exportdialog.ui:327
#, kde-format
msgid "All fields are quoted in the output."
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, cboQuote)
#: dialogs/exportdialog.ui:331
#, kde-format
msgid "\""
msgstr "\""

#. i18n: ectx: property (text), item, widget (QComboBox, cboQuote)
#: dialogs/exportdialog.ui:336
#, kde-format
msgid "'"
msgstr "'"

#: dialogs/historydialog.cpp:105 export/totalsastext.cpp:87
#, kde-format
msgid "Task"
msgstr "ਕੰਮ"

#: dialogs/historydialog.cpp:105
#, kde-format
msgid "StartTime"
msgstr "ਸ਼ੁਰੂਸਮਾਂ"

#: dialogs/historydialog.cpp:105
#, kde-format
msgid "EndTime"
msgstr "ਅੰਤਸਮਾਂ"

#: dialogs/historydialog.cpp:105
#, fuzzy, kde-format
#| msgid "Comma"
msgid "Comment"
msgstr "ਕਾਮਾ"

#: dialogs/historydialog.cpp:146
#, kde-format
msgctxt "@info:whatsthis"
msgid "You can change this task's comment, start time and end time."
msgstr ""

#: dialogs/historydialog.cpp:197 dialogs/historydialog.cpp:217
#, kde-format
msgid "This is not a valid Date/Time."
msgstr "ਇਹ ਠੀਕ ਸਮਾਂ/ਮਿਤੀ ਨਹੀਂ ਹੈ।"

#: dialogs/historydialog.cpp:273
#, kde-format
msgid "Please select a task to delete."
msgstr ""

#. i18n: ectx: property (windowTitle), widget (QDialog, HistoryDialog)
#: dialogs/historydialog.ui:14
#, kde-format
msgid "Edit History"
msgstr "ਅਤੀਤ ਸੋਧ"

#: dialogs/taskpropertiesdialog.cpp:62
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"<p>Enter the name of the task here. You can choose it freely.</p>\n"
"<p><i>Example:</i> phone with mother</p>"
msgstr ""

#: dialogs/taskpropertiesdialog.cpp:73
#, kde-format
msgctxt "@title:group"
msgid "Auto Tracking"
msgstr ""

#: dialogs/taskpropertiesdialog.cpp:90
#, kde-format
msgctxt "order number of desktop: 1, 2, ..."
msgid "%1."
msgstr ""

#: export/export.cpp:63
#, kde-format
msgid "Could not open \"%1\"."
msgstr ""

#: export/totalsastext.cpp:84
#, kde-format
msgid "Task Totals"
msgstr "ਕੁੱਲ ਕੰਮ"

#: export/totalsastext.cpp:87
#, kde-format
msgid "Time"
msgstr "ਸਮਾਂ"

#: export/totalsastext.cpp:120
#, kde-format
msgctxt "total time of all tasks"
msgid "Total"
msgstr "ਕੁੱਲ"

#: export/totalsastext.cpp:122
#, kde-format
msgid "No tasks."
msgstr "ਕੋਈ ਕੰਮ ਨਹੀਂ ਹੈ।"

#: idletimedetector.cpp:78
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Apply the idle time since %1 to all active\n"
"timers and keep them running."
msgstr ""

#: idletimedetector.cpp:81
#, fuzzy, kde-format
#| msgid "Continue timing."
msgctxt "@action:button"
msgid "Continue Timing"
msgstr "ਲਗਾਤਾਰ ਸਮਾਂ ਹੈ।"

#: idletimedetector.cpp:83
#, kde-format
msgctxt "@info:tooltip"
msgid "Stop timing and revert back to the time at %1"
msgstr ""

#: idletimedetector.cpp:84
#, kde-format
msgctxt "@action:button"
msgid "Revert Timing"
msgstr ""

#: idletimedetector.cpp:88
#, kde-format
msgid "Desktop has been idle since %1. What do you want to do?"
msgstr ""

#. i18n: ectx: Menu (clock)
#: ktimetrackerui.rc:19
#, kde-format
msgid "&Clock"
msgstr "ਘੜੀ(&C)"

#. i18n: ectx: Menu (task)
#: ktimetrackerui.rc:26
#, kde-format
msgid "&Task"
msgstr "ਕੰਮ(&T)"

#. i18n: ectx: Menu (settings)
#: ktimetrackerui.rc:36
#, kde-format
msgid "&Settings"
msgstr "ਸੈਟਿੰਗ(&S)"

#. i18n: ectx: ToolBar (mainToolBar)
#: ktimetrackerui.rc:41
#, kde-format
msgid "Main Toolbar"
msgstr "ਮੁੱਖ ਟੂਲਬਾਰ"

#. i18n: ectx: ToolBar (taskToolBar)
#: ktimetrackerui.rc:50
#, kde-format
msgid "Tasks"
msgstr "ਕੰਮ"

#: main.cpp:100 model/event.cpp:167 model/eventsmodel.cpp:126
#, kde-format
msgid "KTimeTracker"
msgstr "ਕੇਸਮਾਂ-ਟਰੈਕਰ"

#: main.cpp:102
#, kde-format
msgid "KDE Time tracker tool"
msgstr "ਕੇਡੀਈ ਸਮਾਂ-ਖੋਜ ਸੰਦ"

#: main.cpp:104
#, kde-format
msgid "Copyright © 1997-2019 KTimeTracker developers"
msgstr ""

#: main.cpp:108
#, kde-format
msgctxt "@info:credit"
msgid "Alexander Potashev"
msgstr ""

#: main.cpp:109
#, fuzzy, kde-format
#| msgid "Current Maintainer"
msgctxt "@info:credit"
msgid "Current Maintainer (since 2019)"
msgstr "ਮੌਜੂਦਾ ਪ੍ਰਬੰਧਕ"

#: main.cpp:111
#, kde-format
msgctxt "@info:credit"
msgid "Thorsten Stärk"
msgstr ""

#: main.cpp:112
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer (2006-2012)"
msgstr ""

#: main.cpp:114
#, kde-format
msgctxt "@info:credit"
msgid "Mark Bucciarelli"
msgstr ""

#: main.cpp:115
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer (2005-2006)"
msgstr ""

#: main.cpp:117
#, kde-format
msgctxt "@info:credit"
msgid "Jesper Pedersen"
msgstr ""

#: main.cpp:118
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer (2000-2005)"
msgstr ""

#: main.cpp:120
#, kde-format
msgctxt "@info:credit"
msgid "Sirtaj Singh Kang"
msgstr ""

#: main.cpp:121
#, fuzzy, kde-format
#| msgid "Original Author"
msgctxt "@info:credit"
msgid "Original Author"
msgstr "ਅਸਲੀ ਲੇਖਕ"

#: main.cpp:123
#, kde-format
msgctxt "@info:credit"
msgid "Mathias Soeken"
msgstr ""

#: main.cpp:124
#, kde-format
msgctxt "@info:credit"
msgid "Developer (in 2007)"
msgstr ""

#: main.cpp:126
#, kde-format
msgctxt "@info:credit"
msgid "Kalle Dalheimer"
msgstr ""

#: main.cpp:127
#, kde-format
msgctxt "@info:credit"
msgid "Developer (1999-2000)"
msgstr ""

#: main.cpp:129
#, kde-format
msgctxt "@info:credit"
msgid "Allen Winter"
msgstr ""

#: main.cpp:130 main.cpp:133
#, kde-format
msgctxt "@info:credit"
msgid "Developer"
msgstr ""

#: main.cpp:132
#, kde-format
msgctxt "@info:credit"
msgid "David Faure"
msgstr ""

#: main.cpp:144
#, kde-format
msgctxt "@info:shell"
msgid "Path or URL to iCalendar file to open."
msgstr ""

#: mainwindow.cpp:52
#, fuzzy, kde-format
#| msgid "KTimeTracker"
msgctxt "@action:inmenu"
msgid "Configure KTimeTracker..."
msgstr "ਕੇਸਮਾਂ-ਟਰੈਕਰ"

#: mainwindow.cpp:152 tray.cpp:90
#, kde-format
msgid "No active tasks"
msgstr "ਕੋਈ ਸਰਗਰਮ ਕੰਮ ਨਹੀਂ"

#: mainwindow.cpp:162 tray.cpp:110
#, fuzzy, kde-format
#| msgid ", "
msgctxt "separator between task names"
msgid ", "
msgstr ", "

#: model/tasksmodel.cpp:33
#, fuzzy, kde-format
#| msgid "Task Name"
msgctxt "@title:column"
msgid "Task Name"
msgstr "ਕੰਮ ਨਾਂ"

#: model/tasksmodel.cpp:34
#, fuzzy, kde-format
#| msgid "Session Time"
msgctxt "@title:column"
msgid "Session Time"
msgstr "ਸ਼ੈਸ਼ਨ ਸਮਾਂ"

#: model/tasksmodel.cpp:35
#, fuzzy, kde-format
#| msgid "Time"
msgctxt "@title:column"
msgid "Time"
msgstr "ਸਮਾਂ"

#: model/tasksmodel.cpp:36
#, fuzzy, kde-format
#| msgid "Total Session Time"
msgctxt "@title:column"
msgid "Total Session Time"
msgstr "ਕੁੱਲ ਸ਼ੈਸ਼ਨ ਸਮਾਂ"

#: model/tasksmodel.cpp:37
#, fuzzy, kde-format
#| msgid "Total Time"
msgctxt "@title:column"
msgid "Total Time"
msgstr "ਕੁੱਲ ਸਮਾਂ"

#: model/tasksmodel.cpp:38
#, fuzzy, kde-format
#| msgid "Priority"
msgctxt "@title:column"
msgid "Priority"
msgstr "ਤਰਜੀਹ"

#: model/tasksmodel.cpp:39
#, fuzzy, kde-format
#| msgid "Percent Complete"
msgctxt "@title:column"
msgid "Percent Complete"
msgstr "ਫੀਸਦੀ ਮੁਕੰਮਲ"

#: model/tasksmodel.cpp:189 model/tasksmodel.cpp:282
#, kde-format
msgctxt "@info:whatsthis"
msgid "The task name is what you call the task, it can be chosen freely."
msgstr ""

#: model/tasksmodel.cpp:193 model/tasksmodel.cpp:286
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The session time is the time since you last chose \"Start New Session\"."
msgstr ""

#: model/tasksmodel.cpp:290
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The total session time is the session time of this task and all its subtasks."
msgstr ""

#: model/tasksmodel.cpp:294
#, kde-format
msgctxt "@info:whatsthis"
msgid "The total time is the time of this task and all its subtasks."
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_enabled)
#: settings/cfgbehavior.ui:17
#, kde-format
msgid "Detect desktop as idle after:"
msgstr ""

#. i18n: ectx: property (suffix), widget (QSpinBox, kcfg_autoSavePeriod)
#. i18n: ectx: property (suffix), widget (QSpinBox, kcfg_period)
#: settings/cfgbehavior.ui:24 settings/cfgstorage.ui:36
#, kde-format
msgid " min"
msgstr " ਮਿੰਟ"

#. i18n: ectx: property (text), widget (QLabel, label)
#: settings/cfgbehavior.ui:37
#, kde-format
msgid "Minimum desktop active time:"
msgstr ""

#. i18n: ectx: property (suffix), widget (QSpinBox, kcfg_minActiveTime)
#: settings/cfgbehavior.ui:44
#, kde-format
msgid " sec"
msgstr " ਸਕਿੰਟ"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_promptDelete)
#: settings/cfgbehavior.ui:57
#, kde-format
msgid "Prompt before deleting tasks"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QCheckBox, kcfg_uniTasking)
#: settings/cfgbehavior.ui:64
#, kde-format
msgid ""
"Unitasking - allow only one task to be timed at a time. Does not stop any "
"timer."
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_uniTasking)
#: settings/cfgbehavior.ui:67
#, kde-format
msgid "Allow only one timer at a time"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_trayIcon)
#: settings/cfgbehavior.ui:74
#, kde-format
msgid "Place an icon to the system tray"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: settings/cfgdisplay.ui:29
#, kde-format
msgctxt "title of group box, general options"
msgid "General"
msgstr "ਆਮ"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_decimalFormat)
#: settings/cfgdisplay.ui:35
#, kde-format
msgid "Decimal number format"
msgstr "ਦਸ਼ਮਲਵ ਨੰਬਰ ਫਾਰਮੈਟ"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, kcfg_configPDA)
#: settings/cfgdisplay.ui:42
#, kde-format
msgid ""
"Choose this if you have a touchscreen and your screen real estate is "
"precious. It will disable the search bar and every click will pop up a "
"context menu."
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_configPDA)
#: settings/cfgdisplay.ui:45
#, fuzzy, kde-format
#| msgid "Confirmation Required"
msgctxt ""
"Choose this if you have a touchscreen and your screen real estate is "
"precious."
msgid "Configuration for PDA"
msgstr "ਲਾਜ਼ਮੀ ਤਸਦੀਕ"

#. i18n: ectx: property (title), widget (QGroupBox, windowTitleGroupBox)
#: settings/cfgdisplay.ui:55
#, kde-format
msgid "Window Title"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_windowTitleCurrentFile)
#: settings/cfgdisplay.ui:64
#, kde-format
msgid "Currently opened file"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, kcfg_windowTitleCurrentTask)
#: settings/cfgdisplay.ui:71
#, fuzzy, kde-format
#| msgid "Total Session Time"
msgid "Current Task and Session Time"
msgstr "ਕੁੱਲ ਸ਼ੈਸ਼ਨ ਸਮਾਂ"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: settings/cfgdisplay.ui:81
#, fuzzy, kde-format
#| msgid "Columns displayed:"
msgid "Columns Displayed"
msgstr "ਕਾਲਮ ਵੇਖਾਓ:"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_displaySessionTime)
#: settings/cfgdisplay.ui:87
#, kde-format
msgid "Session time"
msgstr "ਸ਼ੈਸ਼ਨ ਸਮਾਂ"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_displayTime)
#: settings/cfgdisplay.ui:94
#, kde-format
msgid "Cumulative task time"
msgstr "ਆਵਰਤੀ ਕੰਮ ਸਮਾਂ"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_displayTotalSessionTime)
#: settings/cfgdisplay.ui:101
#, kde-format
msgid "Total session time"
msgstr "ਕੁੱਲ ਸ਼ੈਸ਼ਨ ਸਮਾਂ"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_displayTotalTime)
#: settings/cfgdisplay.ui:108
#, kde-format
msgid "Total task time"
msgstr "ਕੁੱਲ ਕੰਮ ਸਮਾਂ"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_displayPriority)
#: settings/cfgdisplay.ui:115
#, kde-format
msgid "Priority"
msgstr "ਤਰਜੀਹ"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_displayPercentComplete)
#: settings/cfgdisplay.ui:122
#, kde-format
msgid "Percent complete"
msgstr "ਫੀਸਦੀ ਮੁਕੰਮਲ"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_autoSave)
#: settings/cfgstorage.ui:29
#, fuzzy, kde-format
#| msgid "Save tasks every"
msgid "Save tasks every:"
msgstr "ਹਰ ਕੰਮ ਸੰਭਾਲੋ"

#: taskview.cpp:118
#, kde-format
msgid ""
"Error storing new task. Your changes were not saved. Make sure you can edit "
"your iCalendar file. Also quit all applications using this file and remove "
"any lock file related to its name from ~/.kde/share/apps/kabc/lock/ "
msgstr ""

#: taskview.cpp:214
#, kde-format
msgid ""
"Your virtual desktop number is too high, desktop tracking will not work."
msgstr ""

#: taskview.cpp:290
#, kde-format
msgctxt "@info:progress"
msgid "Stopping timers..."
msgstr ""

#: taskview.cpp:291
#, kde-format
msgid "Cancel"
msgstr ""

#: taskview.cpp:361 taskview.cpp:445
#, kde-format
msgid "Unnamed Task"
msgstr "ਬੇਨਾਮ ਕੰਮ"

#: taskview.cpp:380
#, kde-format
msgid ""
"Error storing new task. Your changes were not saved. Make sure you can edit "
"your iCalendar file. Also quit all applications using this file and remove "
"any lock file related to its name from ~/.kde/share/apps/kabc/lock/"
msgstr ""

#: taskview.cpp:422
#, fuzzy, kde-format
#| msgid "New Sub Task"
msgctxt "@title:window"
msgid "New Sub Task"
msgstr "ਨਵਾਂ ਹੋਰ ਕੰਮ"

#: taskview.cpp:440
#, fuzzy, kde-format
#| msgid "Edit Task"
msgctxt "@title:window"
msgid "Edit Task"
msgstr "ਕੰਮ ਸੋਧ"

#: taskview.cpp:474 taskview.cpp:515 taskview.cpp:537
#, kde-format
msgid "No task selected."
msgstr "ਕੋਈ ਕੰਮ ਚੁਣਿਆ ਨਹੀਂ ਹੈ"

#: taskview.cpp:520
#, kde-format
msgid ""
"Are you sure you want to delete the selected task and its entire history?\n"
"Note: All subtasks and their history will also be deleted."
msgstr ""

#: taskview.cpp:524
#, fuzzy, kde-format
#| msgid "Deleting Task(s)"
msgctxt "@title:window"
msgid "Deleting Task"
msgstr "ਕੰਮ ਹਟਾਏ ਜਾ ਰਹੇ ਹਨ"

#: timetrackerstorage.cpp:140
#, kde-format
msgid "Error loading \"%1\": could not find parent (uid=%2)"
msgstr ""

#: timetrackerstorage.cpp:243
#, fuzzy, kde-format
#| msgid "Could not save. Disk full ?"
msgctxt "%1=lock file path"
msgid "Could not write lock file \"%1\". Disk full?"
msgstr "ਸੰਭਾਲੀ ਨਹੀਂ ਜਾ ਸਕੀ। ਡਿਸਕ ਭਰ ਗਈ?"

#: timetrackerstorage.cpp:251
#, kde-format
msgctxt "%1=destination file path/URL"
msgid "Failed to save iCalendar file as \"%1\"."
msgstr ""

#: timetrackerwidget.cpp:154
#, fuzzy, kde-format
#| msgid "Start &New Session"
msgctxt "@action:inmenu"
msgid "Start &New Session"
msgstr "ਨਵਾਂ ਸ਼ੈਸ਼ਨ ਸ਼ੁਰੂ ਕਰੋ(&N)"

#: timetrackerwidget.cpp:155
#, fuzzy, kde-format
#| msgid "Starts a new session"
msgctxt "@info:tooltip"
msgid "Starts a new session"
msgstr "ਨਵਾਂ ਸ਼ੈਸ਼ਨ ਸ਼ੁਰੂ ਕਰੋ"

#: timetrackerwidget.cpp:157
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"This will reset the session time to 0 for all tasks, to start a new session, "
"without affecting the totals."
msgstr ""

#: timetrackerwidget.cpp:165
#, fuzzy, kde-format
#| msgid "Edit History..."
msgctxt "@action:inmenu"
msgid "Edit History..."
msgstr "ਅਤੀਤ ਸੋਧ..."

#: timetrackerwidget.cpp:166
#, kde-format
msgctxt "@info:tooltip"
msgid "Edits history of all tasks of the current document"
msgstr ""

#: timetrackerwidget.cpp:168
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"A window will be opened where you can change start and stop times of tasks "
"or add a comment to them."
msgstr ""

#: timetrackerwidget.cpp:178
#, fuzzy, kde-format
#| msgid "&Reset All Times"
msgctxt "@action:inmenu"
msgid "&Reset All Times"
msgstr "ਸਭ ਸਮੇੰ ਮੁੜ-ਸੈਟ(&R)"

#: timetrackerwidget.cpp:179
#, fuzzy, kde-format
#| msgid "Resets all times"
msgctxt "@info:tooltip"
msgid "Resets all times"
msgstr "ਸਭ ਸਮੇੰ ਮੁੜ-ਸੈਟ"

#: timetrackerwidget.cpp:181
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"This will reset the session and total time to 0 for all tasks, to restart "
"from scratch."
msgstr ""

#: timetrackerwidget.cpp:188
#, fuzzy, kde-format
#| msgid "&Start"
msgctxt "@action:inmenu"
msgid "&Start"
msgstr "ਸ਼ੁਰੂ(&S)"

#: timetrackerwidget.cpp:189
#, fuzzy, kde-format
#| msgid "Selected Task"
msgctxt "@info:tooltip"
msgid "Starts timing for selected task"
msgstr "ਚੁਣਿਆ ਕੰਮ"

#: timetrackerwidget.cpp:191
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"This will start timing for the selected task.\n"
"It is even possible to time several tasks simultaneously.\n"
"\n"
"You may also start timing of tasks by double clicking the left mouse button "
"on a given task. This will, however, stop timing of other tasks."
msgstr ""

#: timetrackerwidget.cpp:205
#, fuzzy, kde-format
#| msgid "S&top"
msgctxt "@action:inmenu"
msgid "S&top"
msgstr "ਰੋਕੋ(&t)"

#: timetrackerwidget.cpp:206
#, fuzzy, kde-format
#| msgid "Selected Task"
msgctxt "@info:tooltip"
msgid "Stops timing of the selected task"
msgstr "ਚੁਣਿਆ ਕੰਮ"

#: timetrackerwidget.cpp:207
#, fuzzy, kde-format
#| msgid "Selected Task"
msgctxt "@info:whatsthis"
msgid "Stops timing of the selected task"
msgstr "ਚੁਣਿਆ ਕੰਮ"

#: timetrackerwidget.cpp:214
#, kde-format
msgctxt "@action:inmenu"
msgid "Focus on Searchbar"
msgstr ""

#: timetrackerwidget.cpp:215
#, kde-format
msgctxt "@info:tooltip"
msgid "Sets the focus on the searchbar"
msgstr ""

#: timetrackerwidget.cpp:216
#, kde-format
msgctxt "@info:whatsthis"
msgid "Sets the focus on the searchbar"
msgstr ""

#: timetrackerwidget.cpp:223
#, fuzzy, kde-format
#| msgid "All Times"
msgctxt "@action:inmenu"
msgid "Stop &All Timers"
msgstr "ਸਭ ਸਮੇਂ"

#: timetrackerwidget.cpp:224
#, kde-format
msgctxt "@info:tooltip"
msgid "Stops all of the active timers"
msgstr ""

#: timetrackerwidget.cpp:225
#, kde-format
msgctxt "@info:whatsthis"
msgid "Stops all of the active timers"
msgstr ""

#: timetrackerwidget.cpp:233
#, kde-format
msgctxt "@action:inmenu"
msgid "Track Active Applications"
msgstr ""

#: timetrackerwidget.cpp:235
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Auto-creates and updates tasks when the focus of the current window has "
"changed"
msgstr ""

#: timetrackerwidget.cpp:238
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"If the focus of a window changes for the first time when this action is "
"enabled, a new task will be created with the title of the window as its name "
"and will be started. If there already exists such an task it will be started."
msgstr ""

#: timetrackerwidget.cpp:249
#, fuzzy, kde-format
#| msgid "&New Task..."
msgctxt "@action:inmenu"
msgid "&New Task..."
msgstr "ਨਵਾਂ ਕੰਮ(&N)..."

#: timetrackerwidget.cpp:250
#, kde-format
msgctxt "@info:tooltip"
msgid "Creates new top level task"
msgstr ""

#: timetrackerwidget.cpp:251
#, kde-format
msgctxt "@info:whatsthis"
msgid "This will create a new top level task."
msgstr ""

#: timetrackerwidget.cpp:259
#, fuzzy, kde-format
#| msgid "New &Subtask..."
msgctxt "@action:inmenu"
msgid "New &Subtask..."
msgstr "ਨਵਾਂ ਹੋਰ ਕੰਮ(&S)..."

#: timetrackerwidget.cpp:260
#, kde-format
msgctxt "@info:tooltip"
msgid "Creates a new subtask to the current selected task"
msgstr ""

#: timetrackerwidget.cpp:261
#, kde-format
msgctxt "@info:whatsthis"
msgid "This will create a new subtask to the current selected task."
msgstr ""

#: timetrackerwidget.cpp:269
#, fuzzy, kde-format
#| msgid "&Delete"
msgctxt "@action:inmenu"
msgid "&Delete"
msgstr "ਹਟਾਓ(&D)"

#: timetrackerwidget.cpp:270
#, fuzzy, kde-format
#| msgid "Selected Task"
msgctxt "@info:tooltip"
msgid "Deletes selected task"
msgstr "ਚੁਣਿਆ ਕੰਮ"

#: timetrackerwidget.cpp:271
#, kde-format
msgctxt "@info:whatsthis"
msgid "This will delete the selected task(s) and all subtasks."
msgstr ""

#: timetrackerwidget.cpp:279
#, kde-format
msgctxt "@action:inmenu"
msgid "&Properties"
msgstr ""

#: timetrackerwidget.cpp:280
#, fuzzy, kde-format
#| msgid "Selected Task"
msgctxt "@info:tooltip"
msgid "Edit name or description for selected task"
msgstr "ਚੁਣਿਆ ਕੰਮ"

#: timetrackerwidget.cpp:282
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"This will bring up a dialog box where you may edit the parameters for the "
"selected task."
msgstr ""

#: timetrackerwidget.cpp:291
#, fuzzy, kde-format
#| msgid "Edit Task"
msgctxt "@action:inmenu"
msgid "Edit &Time..."
msgstr "ਕੰਮ ਸੋਧ"

#: timetrackerwidget.cpp:292
#, fuzzy, kde-format
#| msgid "Selected Task"
msgctxt "@info:tooltip"
msgid "Edit time for selected task"
msgstr "ਚੁਣਿਆ ਕੰਮ"

#: timetrackerwidget.cpp:294
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"This will bring up a dialog box where you may edit the times for the "
"selected task."
msgstr ""

#: timetrackerwidget.cpp:303
#, fuzzy, kde-format
#| msgid "Percent Complete"
msgctxt "@action:inmenu"
msgid "&Mark as Complete"
msgstr "ਫੀਸਦੀ ਮੁਕੰਮਲ"

#: timetrackerwidget.cpp:311
#, kde-format
msgctxt "@action:inmenu"
msgid "&Mark as Incomplete"
msgstr ""

#: timetrackerwidget.cpp:319
#, fuzzy, kde-format
#| msgid "&Export"
msgctxt "@action:inmenu"
msgid "&Export..."
msgstr "ਐਕਸਪੋਰਟ(&E)"

#: timetrackerwidget.cpp:326
#, kde-format
msgctxt "@action:inmenu"
msgid "Import Tasks From &Planner..."
msgstr ""

#: timetrackerwidget.cpp:338
#, kde-format
msgctxt "@action:inmenu"
msgid "Show Searchbar"
msgstr ""

#: timetrackerwidget.cpp:471
#, fuzzy, kde-format
#| msgid "Behavior"
msgctxt "@title:tab"
msgid "Behavior"
msgstr "ਵਿਵਹਾਰ"

#: timetrackerwidget.cpp:474
#, kde-format
msgctxt "@title:tab"
msgid "Appearance"
msgstr ""

#: timetrackerwidget.cpp:477
#, fuzzy, kde-format
#| msgid "Storage"
msgctxt "@title:tab"
msgid "Storage"
msgstr "ਸੰਭਾਲਣ"

#: timetrackerwidget.cpp:514
#, fuzzy, kde-format
#| msgid "New Task"
msgctxt "@title:window"
msgid "New Task"
msgstr "ਨਵਾਂ ਕੰਮ"

#: timetrackerwidget.cpp:593
#, kde-format
msgctxt "@info in message box"
msgid ""
"There is no history yet. Start and stop a task and you will have an entry in "
"your history."
msgstr ""

#: timetrackerwidget.cpp:603
#, kde-format
msgid ""
"Do you really want to reset the time to zero for all tasks? This will delete "
"the entire history."
msgstr ""

#: timetrackerwidget.cpp:605
#, fuzzy, kde-format
#| msgid "Confirmation Required"
msgctxt "@title:window"
msgid "Confirmation Required"
msgstr "ਲਾਜ਼ਮੀ ਤਸਦੀਕ"

#: timetrackerwidget.cpp:606
#, fuzzy, kde-format
#| msgid "Reset All Times"
msgctxt "@action:button"
msgid "Reset All Times"
msgstr "ਸਭ ਸਮੇੰ ਮੁੜ-ਸੈਟ"

#: timetrackerwidget.cpp:795
#, kde-format
msgid "Save failed, most likely because the file could not be locked."
msgstr ""

#: timetrackerwidget.cpp:797
#, kde-format
msgid "Could not modify calendar resource."
msgstr ""

#: timetrackerwidget.cpp:799
#, kde-format
msgid "Out of memory--could not create object."
msgstr ""

#: timetrackerwidget.cpp:801
#, kde-format
msgid "UID not found."
msgstr "UID ਨਹੀਂ ਲੱਭਿਆ ਹੈ।"

#: timetrackerwidget.cpp:803
#, kde-format
msgid "Invalidate date--format is YYYY-MM-DD."
msgstr "ਗਲਤ ਮਿਤੀ--ਫਾਰਮੈਟ YYYY-MM-DD ਹੈ।"

#: timetrackerwidget.cpp:805
#, kde-format
msgid "Invalid time--format is YYYY-MM-DDTHH:MM:SS."
msgstr "ਗਲਤ ਸਮਾਂ--ਫਾਰਮੈਟ YYYY-MM-DD:MM:SS ਹੈ।"

#: timetrackerwidget.cpp:807
#, kde-format
msgid "Invalid task duration--must be greater than zero."
msgstr ""

#: timetrackerwidget.cpp:809
#, kde-format
msgid "Invalid error number: %1"
msgstr ""

#: timetrackerwidget.cpp:1038
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"This is ktimetracker, KDE's program to help you track your time. Best, start "
"with creating your first task - enter it into the field where you see "
"\"Search or add task\"."
msgstr ""

#: timetrackerwidget.cpp:1045
#, kde-format
msgctxt "@info:whatsthis"
msgid "You have already created a task. You can now start and stop timing."
msgstr ""

#: tray.cpp:95
#, fuzzy, kde-format
#| msgid ", ..."
msgctxt "ellipsis to truncate long list of tasks"
msgid ", ..."
msgstr ", ..."

#: treeviewheadercontextmenu.cpp:46
#, fuzzy, kde-format
#| msgid "Columns"
msgctxt "@title:menu"
msgid "Columns"
msgstr "ਕਾਲਮ"

#: widgets/searchline.cpp:37
#, kde-format
msgctxt "@info:placeholder"
msgid "Search or add task"
msgstr ""

#: widgets/searchline.cpp:39
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"This is a combined field. As long as you do not press Enter, it acts as a "
"filter. Then, only tasks that match your input are shown. As soon as you "
"press Enter, your input is used as name to create a new task."
msgstr ""

#: widgets/taskswidget.cpp:143
#, fuzzy, kde-format
#| msgid "%1 %"
msgctxt "@item:inmenu Task progress"
msgid "%1 %"
msgstr "%1 %"

#: widgets/taskswidget.cpp:154
#, kde-format
msgctxt "@item:inmenu Task priority"
msgid "unspecified"
msgstr ""

#: widgets/taskswidget.cpp:157
#, fuzzy, kde-format
#| msgctxt "combox entry for highest priority"
#| msgid "1 (highest)"
msgctxt "@item:inmenu Task priority"
msgid "1 (highest)"
msgstr "1 (ਸਭ ਤੋਂ ਵੱਧ)"

#: widgets/taskswidget.cpp:160
#, fuzzy, kde-format
#| msgctxt "combox entry for medium priority"
#| msgid "5 (medium)"
msgctxt "@item:inmenu Task priority"
msgid "5 (medium)"
msgstr "5 (ਮੱਧਮ)"

#: widgets/taskswidget.cpp:163
#, fuzzy, kde-format
#| msgctxt "combox entry for lowest priority"
#| msgid "9 (lowest)"
msgctxt "@item:inmenu Task priority"
msgid "9 (lowest)"
msgstr "9 (ਸਭ ਤੋਂ ਘੱਟ)"

#~ msgid "CSV Export"
#~ msgstr "CSV ਐਕਸਪੋਰਟ"

#~ msgid "&Edit..."
#~ msgstr "ਸੋਧ(&E)..."

#, fuzzy
#~| msgid "Edit Task"
#~ msgid "Add/Edit a task"
#~ msgstr "ਕੰਮ ਸੋਧ"

#, fuzzy
#~| msgid "&Delete"
#~ msgid "Delete"
#~ msgstr "ਹਟਾਓ(&D)"

#~ msgid "Could not save."
#~ msgstr "ਸੰਭਾਲੀ ਨਹੀਂ ਜਾ ਸਕੀ।"

#~ msgid "&Import"
#~ msgstr "ਇੰਪੋਰਟ(&I)"

#, fuzzy
#~| msgid "&Export"
#~ msgid "&Export Times..."
#~ msgstr "ਨਿਰਯਾਤ(&E)"

#, fuzzy
#~| msgid "Edit history"
#~ msgid "Export &History..."
#~ msgstr "ਅਤੀਤ ਸੋਧ"

#~ msgid "ktimetracker"
#~ msgstr "ਕੇਸਮਾਂ-ਟਰੈਕਰ"

#~ msgid "&File"
#~ msgstr "ਫਾਇਲ(&F)"

#~ msgid "Delete task <taskid>"
#~ msgstr "<taskid> ਕੰਮ ਹਟਾਓ"

#, fuzzy
#~| msgid "Selected Task"
#~ msgid "Start timer for task <taskid>"
#~ msgstr "ਚੁਣਿਆ ਕੰਮ"

#, fuzzy
#~| msgid "Selected Task"
#~ msgid "Stop timer for task <taskid>"
#~ msgstr "ਚੁਣਿਆ ਕੰਮ"

#~ msgid "task_popup"
#~ msgstr "task_popup"

#~ msgid "File"
#~ msgstr "ਫਾਇਲ"

#~ msgid "Help"
#~ msgstr "ਮੱਦਦ"

#~ msgid "toolBar"
#~ msgstr "ਟੂਲਬਾਰ"

#~ msgid "Load"
#~ msgstr "ਲੋਡ"

#~ msgid "Save"
#~ msgstr "ਸੰਭਾਲੋ"

#~ msgid "Quit"
#~ msgstr "ਬਾਹਰ"

#, fuzzy
#~| msgid "KTimeTracker"
#~ msgid "Configure ktimetracker"
#~ msgstr "ਕੇਸਮਾਂ-ਟਰੈਕਰ"

#~ msgid "Week of %1"
#~ msgstr "%1 ਦਾ ਹਫਤਾ"

#~ msgid "Settings"
#~ msgstr "ਸੈਟਿੰਗ"

#~ msgid "Show"
#~ msgstr "ਵੇਖੋ"

#~ msgid "Hide"
#~ msgstr "ਓਹਲੇ"

#~ msgid "Dialog"
#~ msgstr "ਡਾਈਲਾਗ"

#~ msgid "Untitled"
#~ msgstr "ਬਿਨਾਂ-ਟਾਈਟਲ"

#~ msgid "delete"
#~ msgstr "ਹਟਾਓ"

#~ msgctxt "abbreviation for hours"
#~ msgid " hr. "
#~ msgstr "ਘੰਟੇ"

#~ msgctxt "abbreviation for minutes"
#~ msgid " min. "
#~ msgstr " ਮਿੰਟ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 5"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 4"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 3"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 2"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 1"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 8"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 9"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 6"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 7"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 10"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 12"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 17"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 11"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 16"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 13"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 18"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 14"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 19"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 15"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#, fuzzy
#~| msgid "In Desktop"
#~ msgid "Desktop 20"
#~ msgstr "ਵਿਹੜੇ ਵਿੱਚ"

#~ msgid "Task &name:"
#~ msgstr "ਕੰਮ ਨਾਂ(&n):"

#~ msgid "&Time:"
#~ msgstr "ਸਮਾਂ:"

#, fuzzy
#~| msgid "&Session time: "
#~ msgid "&Session time:"
#~ msgstr "ਸ਼ੈਸ਼ਨ ਸਮਾਂ:"

#, fuzzy
#~| msgid "&Start"
#~ msgid "Start/Stop Timer"
#~ msgstr "ਸ਼ੁਰੂ(&S)"

#~ msgid "Session: %1"
#~ msgstr "ਸ਼ੈਸ਼ਨ: %1"

#, fuzzy
#~| msgid "Total: %1"
#~ msgctxt "total time of all tasks"
#~ msgid "Total: %1"
#~ msgstr "ਕੁੱਲ: %1"

#~ msgid "Session"
#~ msgstr "ਸ਼ੈਸ਼ਨ"

#~ msgid "No hours logged."
#~ msgstr "ਕੋਈ ਘੰਟੇ ਲਾੱਗਆਨ ਨਹੀਂ ਹੈ।"

#~ msgid "Task History"
#~ msgstr "ਕੰਮ ਅਤੀਤ"

#~ msgid "From %1 to %2"
#~ msgstr "%1 ਤੋਂ %2 ਤੱਕ"

#~ msgid "Selected Task"
#~ msgstr "ਚੁਣਿਆ ਕੰਮ"

#~ msgid "Totals only"
#~ msgstr "ਕੇਵਲ ਕੁੱਲ"

#~ msgid "KArm"
#~ msgstr "ਕੇਅਲਾਰਮ"

#~ msgid "Saved successfully"
#~ msgstr "ਠੀਕ ਤਰ੍ਹਾਂ ਸੰਭਾਲਿਆ ਗਿਆ"

#, fuzzy
#~| msgid "Display"
#~ msgctxt "settings page for customizing user interface"
#~ msgid "Display"
#~ msgstr "ਝਲਕ"
